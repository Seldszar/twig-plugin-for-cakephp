<?php
/**
 * Copyright 2013, Alexandre Breteau (http://seldszar.fr)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2013, Alexandre Breteau (http://seldszar.fr)
 * @link          http://seldszar.fr Seldszar.fr
 * @package       Twig.Error
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * Used when an extension cannot be found.
 *
 * @package       Twig.Error
 */
class MissingExtensionException extends CakeException {

	protected $_messageTemplate = 'Extension class %s could not be found.';

}

/**
 * Used when Twig has an error.
 *
 * @package       Twig.Error
 */
class TwigErrorException extends CakeException {
}
